package me.twanl.library.lib;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class InventoryLib {

    /**
     * This methode require XMaterial due to support from 1.8.x to 1.13.x
     * @param inv
     * @param ItemName
     * @param amount
     * @param slot
     * @param itemType
     * @param list
     */
    public static void addItem(Inventory inv, String ItemName, int amount, int slot, String itemType, List<String> list) {
        ItemStack I = new ItemStack(XMaterial.fromString(itemType).parseItem());
        I.setAmount(amount);

        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);

        // Each array in the ArrayList will put into a lore
        if (list != null) {
            ArrayList<String> lore = new ArrayList();
            for (String s : list) {
                lore.add(s);
            }
            IMeta.setLore(lore);
        }

        I.setItemMeta(IMeta);
        inv.setItem(slot, I);
    }

//    !! EXAMPLE !! If you create an inventory and you want to add items into it than you can use additem instead of using Itemstack and all those methode's
//
//    List<String> lore = new ArrayList<>();
//    lore.add("Shiney Egg")
//
//    if you want to use an item that contain data/byte than you want to use XMaterial.<item>.toString()
//
//    InventoryLib.addItem(I, "Golden Egg", 5, 1, Material.EGG.toString(), lore)

}
